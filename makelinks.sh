#!/bin/bash

make_link() {
  local new="$1"
  local target="$2"
  if [ -L "$new" ] ; then
    lnk=`readlink "$new"`
     if [ ! "$lnk" = "$target" ] ; then
        echo "$dotfile => $target"
        ln -sni "$target"  "$dotfile"
     fi
  else
     echo "$new => $target"
     if [ -e "$new" ] ; then
        mv "$new" $DOTFILES_BACK
        echo "$new Backup"
     fi
     ln -sni "$target"  "$new"
  fi
}

DOTFILES=`dirname $0`/dotfiles
DOTFILES_BACK=~/dotfiles_backup

if [ ! -d $DOTFILES_BACK ] ; then
   mkdir $DOTFILES_BACK
fi

find $DOTFILES -type f -print0 | while IFS= read -r -d $'\0' file; do
   target="`readlink -f "$file"`"
   dest="$HOME/.`basename $file`"
   make_link "$dest"  "$target"
done

BINDIR=`dirname $0`/bin
BINDIR=`readlink -f $BINDIR`

if [ -L ~/bin ] ; then
 lnk=`readlink ~/bin`
 if [ ! $lnk = $BINDIR ] ; then
    ln -sni $BINDIR ~/bin
 fi
else
    ln -sni $BINDIR ~/bin
fi


ST_DIR=`dirname $0`/sublime-text-2/Packages/User
ST_DIR=`readlink -f $ST_DIR`

ST_DEST=~/.config/sublime-text-2/Packages/User

mkdir -p $ST_DIR

find $ST_DIR -type f -print0 | while IFS= read -r -d $'\0' file; do
   target="`readlink -f "$file"`"
   dest="$ST_DEST/`basename "$file"`"
   make_link "$dest"  "$target"
done

